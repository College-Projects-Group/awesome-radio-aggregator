package awradiogroup.aggregator;

public class RadioStation {
    private int mImageResource ;
    private String mName;
    private String mUrl;
    private String mOriginalUrl;

    public RadioStation(int imageResource, String name, String url) {
        mImageResource  = imageResource;
        mName = name;
        mUrl = url;
        mOriginalUrl = mUrl;
    }

    public void changeImage() {
        if (mImageResource  == R.drawable.ic_music_note) {
            mImageResource  = R.drawable.ic_play;
        } else {
            mImageResource  = R.drawable.ic_music_note;
        }
    }

    public void changeSelected() {
        if (mUrl  == "Selected") {
            mUrl  = mOriginalUrl;
        } else {
            mUrl  = "Selected";
        }
    }

    public int getImageResource() {
        return mImageResource;
    }

    public String getStationName() {
        return mName;
    }

    public String getStationUrl() {
        return mUrl;
    }

    public String getStationOriginalUrl() {
        return mOriginalUrl;
    }
}
