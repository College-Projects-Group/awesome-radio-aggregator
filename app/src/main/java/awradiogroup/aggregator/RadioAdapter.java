package awradiogroup.aggregator;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.RadioViewHolder> {
    private ArrayList<RadioStation> mStationsList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDeleteClick(int positoin);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class RadioViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mNameView;
        public TextView mUrlView;
        public ImageView mDeleteImage;

        public RadioViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView);
            mNameView = itemView.findViewById(R.id.nameView);
            mUrlView = itemView.findViewById(R.id.urlView);
            mDeleteImage = itemView.findViewById(R.id.image_delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public RadioAdapter(ArrayList<RadioStation> stationsList) {
        mStationsList = stationsList;
    }

    @Override
    public RadioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.radio_station_template, parent, false);
        RadioViewHolder evh = new RadioViewHolder(v, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(RadioViewHolder holder, int position) {
        RadioStation currentItem = mStationsList.get(position);

        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mNameView.setText(currentItem.getStationName());
        holder.mUrlView.setText(currentItem.getStationUrl());
    }

    @Override
    public int getItemCount() {
        return mStationsList.size();
    }
}