package awradiogroup.aggregator;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements OnClickListener {

    private ArrayList<RadioStation> mStationsList;

    private RecyclerView mRecyclerView;
    private RadioAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public String station;
    public String url;
    private int positionPlaying = -1;
    private int positionSelected = -1;


    private Button buttonAdd;
    private Button buttonPlay;
    private Button buttonStopPlay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUIElements();
        createExampleList();
        buildRecyclerView();
    }

    public void addItem(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        Context context = MainActivity.this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText newStationName = new EditText(context);
        newStationName.setHint("Title");
        layout.addView(newStationName);

        final EditText newStationUrl = new EditText(context);
        newStationUrl.setHint("Url");
        layout.addView(newStationUrl);

        builder.setTitle("Add a new station");
        builder.setView(layout);


        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mStationsList.add(position, new RadioStation(R.drawable.ic_music_note, newStationName.getText().toString(), newStationUrl.getText().toString()));
                mAdapter.notifyItemChanged(position);
            }
        });
        builder.show();
    }

    public void removeItem(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setCancelable(true);
        builder.setTitle("Remove station");
        builder.setMessage("Are you sure you want to remove this station from list?");

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mStationsList.remove(position);
                mAdapter.notifyItemRemoved(position);
            }
        });
        builder.show();

    }

    public void noStation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setCancelable(true);
        builder.setTitle("There is no station selected");

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.show();

    }


    public void createExampleList() {
        mStationsList = new ArrayList<>();

        mStationsList.add(new RadioStation(R.drawable.ic_music_note, "Luna Loves Us Radio", "http://radio.ponyvillelive.com:8002/stream2?sid=2;stream.nsv"));
        mStationsList.add(new RadioStation(R.drawable.ic_music_note, "Nightwave Plaza", "http://radio.plaza.one/mp3"));

    }

    public void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new RadioAdapter(mStationsList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new RadioAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (station == mStationsList.get(position).getStationName()) {
                } else {
                    if (positionSelected != -1) changeSelected(positionSelected);
                    changeSelected(position);
                    positionSelected = position;
                    station = mStationsList.get(position).getStationName();
                    url = mStationsList.get(position).getStationOriginalUrl();
                }
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }
        });
    }

    public void changeSelected(int position) {
        mStationsList.get(position).changeSelected();
        mAdapter.notifyItemChanged(position);
    }

    public void changeStation(int position) {
        mStationsList.get(position).changeImage();
        mAdapter.notifyItemChanged(position);
    }

    private void initializeUIElements() {

        buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(this);

        buttonPlay = findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(this);

        buttonStopPlay = findViewById(R.id.buttonStopPlay);
        buttonStopPlay.setOnClickListener(this);

    }

    public void onClick(View v) {
        if (v == buttonAdd) {
            addItem(mAdapter.getItemCount());
        } else if (v == buttonPlay && positionSelected != -1) {
            if (positionPlaying != -1) changeStation(positionPlaying);
            changeStation(positionSelected);
            positionPlaying = positionSelected;
            Intent startService = new Intent(this, RadioService.class);
            startService
                    .putExtra("inputStation", station)
                    .putExtra("inputUrl", url);
            ContextCompat.startForegroundService(this, startService);
        } else if (v == buttonPlay && positionSelected == -1) {
            noStation();
        } else if (v == buttonStopPlay) {
            stopService(new Intent(this, RadioService.class));
        }
    }

}
