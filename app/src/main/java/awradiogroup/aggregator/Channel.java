package awradiogroup.aggregator;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class Channel extends Application {

        public static final String CHANNEL_ID = "RadioChannel";

        @Override
        public void onCreate() {
            super.onCreate();

            createNotificationChannel();
        }

        private void createNotificationChannel() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Radio Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );
                serviceChannel.setSound(null,null);

                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }
        }
    }
