package awradiogroup.aggregator;

import android.app.Service;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;

import static awradiogroup.aggregator.Channel.CHANNEL_ID;

public class RadioService extends Service {

    private MediaPlayer player;
    private String station;
    private String url;

    @Override
    public void onCreate() {
        super.onCreate();
        player = new MediaPlayer();
    }

    private void initializeMediaPlayer(String station) {
        player = new MediaPlayer();
        try {
            player.setDataSource(station);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String temp = intent.getStringExtra("inputStation");

        if (!player.isPlaying()) {
            url = intent.getStringExtra("inputUrl");
            initializeMediaPlayer(url);
        }

        if (!temp.equals(station) && player.isPlaying()) {
            player.stop();
            player.release();
            url = intent.getStringExtra("inputUrl");
            initializeMediaPlayer(url);
        }

        if (!player.isPlaying()) {

            station = temp;

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, 0);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("AwRadio Aggregator")
                    .setContentText(station)
                    .setSmallIcon(R.drawable.ic_radio)
                    .setContentIntent(pendingIntent)
                    .build();

            player.prepareAsync();

            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    player.start();
                }
            });


            startForeground(1, notification);
        }
        //do heavy work on a background thread
        //stopSelf();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player.isPlaying()) {
            player.stop();
            player.release();
            player = new MediaPlayer();
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}